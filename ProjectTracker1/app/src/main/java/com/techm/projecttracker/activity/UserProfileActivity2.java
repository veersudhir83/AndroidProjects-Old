package com.techm.projecttracker.activity;

import com.google.gson.Gson;
import com.techm.projecttracker.R;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.pojo.UserPojo;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class UserProfileActivity2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile_activity2);
		String[] myStringArray = new String[] {"User Id          ", "First Name   ", "Last Name    ", "Project      ", "Role            "};
		
		// TO FETCH USER OBJECT FROM PREFERENCES
		SharedPreferences settings = getSharedPreferences(ProjectTrackerConstants.PREFS_NAME, MODE_PRIVATE);
	    String  userObjAsString = settings.getString(ProjectTrackerConstants.EXTRA_USER_OBJ_AS_STRING, null);
	    
	    // TO GET THE USER OBJECT USING GSON LIBRARY
		Gson gS = new Gson();
		UserPojo userObj = gS.fromJson(userObjAsString, UserPojo.class);
		
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "UserProfileActivity2.User Object::" + userObj);
		// TO SET THE USER DETAILS ON THE PROFILE PAGE
		myStringArray[0] = myStringArray[0].toString() + (userObj.getUserId());
		myStringArray[1] = myStringArray[1].toString() + (userObj.getUserFirstName());
		myStringArray[2] = myStringArray[2].toString() + (userObj.getUserLastName());
		myStringArray[3] = myStringArray[3].toString() + (userObj.getUserProject());
		myStringArray[4] = myStringArray[4].toString() + (userObj.getUserProjectRole());
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, 
		        android.R.layout.simple_list_item_1, myStringArray);
		ListView listView = (ListView) findViewById(R.id.lv_user_profile_id);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_profile_activity2, menu);
		return true;
	}

}
