package com.techm.projecttracker.activity;

import com.google.gson.Gson;
import com.techm.projecttracker.R;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.pojo.UserPojo;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class UserProfileActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		
		// TO FETCH USER OBJECT FROM PREFERENCES
		SharedPreferences settings = getSharedPreferences(ProjectTrackerConstants.PREFS_NAME, MODE_PRIVATE);
	    String  userObjAsString = settings.getString(ProjectTrackerConstants.EXTRA_USER_OBJ_AS_STRING, null);
	    
	    ((TextView) findViewById(R.id.tv_user_profile_header_id)).setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
	    
	    // TO GET THE USER OBJECT USING GSON LIBRARY
		Gson gS = new Gson();
		UserPojo userObj = gS.fromJson(userObjAsString, UserPojo.class);
		
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "UserProfileActivity.User Object::" + userObj);
		// TO SET THE USER DETAILS ON THE PROFILE PAGE
		((TextView) findViewById(R.id.tv_user_profile_userid_value_id)).setText(userObj.getUserId());
		((TextView) findViewById(R.id.tv_user_profile_userfname_value_id)).setText(userObj.getUserFirstName());
		((TextView) findViewById(R.id.tv_user_profile_userlname_value_id)).setText(userObj.getUserLastName());
		((TextView) findViewById(R.id.tv_user_profile_userproject_value_id)).setText(userObj.getUserProject());
		((TextView) findViewById(R.id.tv_user_profile_userrole_value_id)).setText(userObj.getUserProjectRole());
		((TextView) findViewById(R.id.tv_user_profile_useremail_value_id)).setText(userObj.getUserEmailAddress());
		((TextView) findViewById(R.id.tv_user_profile_userphone_value_id)).setText(userObj.getUserPhoneNumber());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_profile, menu);
		return true;
	}

}
