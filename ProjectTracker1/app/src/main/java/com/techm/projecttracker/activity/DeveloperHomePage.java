package com.techm.projecttracker.activity;

import com.techm.projecttracker.R;
import com.techm.projecttracker.constants.ProjectTrackerConstants;

import android.os.Bundle;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class DeveloperHomePage extends Activity {

	TableLayout table_layout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_developer_home_page);
		table_layout = (TableLayout) findViewById(R.id.tl_user_story_table_id);
		// Show the Up button in the action bar.
		setupActionBar();
		
		buildTable(4,5);
	}
	
	private void buildTable(int rows, int cols) {

		Log.i(ProjectTrackerConstants.APP_DEBUG_TRACKER, "Here");
		// outer for loop
		for (int i = 1; i <= rows; i++) {

			TableRow row = new TableRow(this);
			row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT));
			Log.i(ProjectTrackerConstants.APP_DEBUG_TRACKER, "In i loop::" + i);
			// inner for loop
			for (int j = 1; j <= cols; j++) {
				Log.i(ProjectTrackerConstants.APP_DEBUG_TRACKER, "In j loop::" + j);
				TextView tv = new TextView(this);
				tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT));
				//tv.setBackgroundResource(R.drawable.cell_shape);
				tv.setPadding(5, 5, 5, 5);
				tv.setText("R " + i + ", C" + j);

				row.addView(tv);

			}

			table_layout.addView(row);

		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.developer_home_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
