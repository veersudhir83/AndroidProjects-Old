package com.techm.projecttracker.activity;

import java.util.ArrayList;

import com.techm.projecttracker.R;
import com.techm.projecttracker.R.layout;
import com.techm.projecttracker.R.menu;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.UserStoryPojo;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.widget.TextView;

public class UserStoryViewActivity extends Activity {
	
	UserStoryPojo userStoryPojo = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_story_view);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Bundle bundle = getIntent().getExtras();
		String userStoryId = bundle.getString(ProjectTrackerConstants.EXTRA_USER_STORY_ID);

        if(userStoryId != null && userStoryId.trim().length() > 0)
        {
        	getUserStoryDetails(userStoryId.trim());
        	
        	if(userStoryPojo != null) {
	            ((TextView) findViewById(R.id.tview_user_story_view_storyid_value_id)).setText(userStoryPojo.getStoryId());
	            ((TextView) findViewById(R.id.tview_user_story_view_storytitle_value_id)).setText(userStoryPojo.getStoryTitle());
	            ((TextView) findViewById(R.id.tview_user_story_view_storydescription_value_id)).setText(userStoryPojo.getStoryDescription());
	            ((TextView) findViewById(R.id.tview_user_story_view_storyowner_value_id)).setText(userStoryPojo.getStoryOwnerName());
	            ((TextView) findViewById(R.id.tview_user_story_view_storypoints_value_id)).setText(userStoryPojo.getStoryPoints());
	            ((TextView) findViewById(R.id.tview_user_story_view_storystatus_value_id)).setText(userStoryPojo.getStoryStatus());
        	}
        }
	}
	
	/**
	 * To return User story details
	 * @return
	 */
	private void getUserStoryDetails(String userStoryId) {
		ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
		userStoryPojo = (UserStoryPojo) fb.getUserStoryDetails(userStoryId);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_story_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
