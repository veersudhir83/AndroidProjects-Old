package com.techm.projecttracker.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectTrackerConstants {
	public static final String APP_DEBUG_TRACKER = "ProjectTracker"; // SHOWS THE APPLICATION NAME IN DEBUG LOGCAT 
	public static final String EXTRA_LOGIN_USER_NAME = "USER_NAME"; // USER FULL NAME EXTRA FROM LOGIN TO HOME PAGE
	public static final String EXTRA_USER_STORY_ID = "STORY_ID"; // USER STORY ID EXTRA FROM HOME PAGE TO VIEW/EDIT USER STORY PAGE
	public static final String EXTRA_TASK_ID = "TASK_ID"; // TASK ID EXTRA FROM HOME PAGE TO VIEW/EDIT TASK PAGE
	public static final String EXTRA_USER_OBJ_AS_STRING = "USER_OBJ"; // USER OBJECT PASSED AS STRING TO NEXT ACTIVITIES
	public static final String PREFS_NAME = "ProjectTrackerPrefsFile"; //Shared preferences to share objects across activities
	public static final List<String> STORY_POINTS_LIST= Arrays.asList ("1", "2", "3", "5", "8", "13");
	public static final List<String> STORY_STATUS_LIST= Arrays.asList ("DEFINED", "IN-PROGRESS", "COMPLETED", "ACCEPTED");
	public static final List<String> TASK_EST_HOURS_LIST= Arrays.asList ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20");
	public static final String ADMIN_LOST_PASSWORD_NUMBER = "9966377798";
}
