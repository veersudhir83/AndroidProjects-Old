package com.techm.projecttracker.pojo;

import java.util.ArrayList;

public class UserStoryPojo {
	private String storyId;
	private String storyTitle;
	private String storyDescription;
	private String storyOwnerId;
	private String storyOwnerName;
	private String storyPoints;
	private String storyStatus;
	private ArrayList<TaskPojo> tasksList;
	
	public String getStoryId() {
		return storyId;
	}
	public void setStoryId(String storyId) {
		this.storyId = storyId;
	}
	public String getStoryTitle() {
		return storyTitle;
	}
	public void setStoryTitle(String storyTitle) {
		this.storyTitle = storyTitle;
	}
	public String getStoryDescription() {
		return storyDescription;
	}
	public void setStoryDescription(String storyDescription) {
		this.storyDescription = storyDescription;
	}
	public String getStoryOwnerId() {
		return storyOwnerId;
	}
	public void setStoryOwnerId(String storyOwnerId) {
		this.storyOwnerId = storyOwnerId;
	}
	public String getStoryPoints() {
		return storyPoints;
	}
	public void setStoryPoints(String storyPoints) {
		this.storyPoints = storyPoints;
	}
	public String getStoryStatus() {
		return storyStatus;
	}
	public void setStoryStatus(String storyStatus) {
		this.storyStatus = storyStatus;
	}
	public String getStoryOwnerName() {
		return storyOwnerName;
	}
	public void setStoryOwnerName(String storyOwnerName) {
		this.storyOwnerName = storyOwnerName;
	}
	public ArrayList<TaskPojo> getTasksList() {
		return tasksList;
	}
	public void setTasksList(ArrayList<TaskPojo> tasksList) {
		this.tasksList = tasksList;
	}
}
