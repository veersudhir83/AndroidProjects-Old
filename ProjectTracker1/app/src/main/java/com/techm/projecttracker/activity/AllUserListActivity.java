package com.techm.projecttracker.activity;

import java.util.ArrayList;
import java.util.List;

import com.techm.projecttracker.R;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.UserPojo;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.os.Build;

public class AllUserListActivity extends Activity {
	
	private List<UserPojo> allUserList = new ArrayList<UserPojo>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_users_list);
		// Show the Up button in the action bar.
		setupActionBar();
		populateUserList();
	}
	
	public void populateUserList() {
		ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
		allUserList = (ArrayList<UserPojo>) fb.getAllUsersAsList();
		fb.close();
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "Fetched all users" + allUserList.size());
		//Using Adapter to populate Users list
		ArrayAdapter<UserPojo> userAdapter = new UserListAdapter();
		ListView userListView = (ListView) findViewById(R.id.lview_all_users_id);
		userListView.setAdapter(userAdapter);
	}
	
	private class UserListAdapter extends ArrayAdapter<UserPojo>{

		public UserListAdapter() {
			super(AllUserListActivity.this, R.layout.user_item_view, allUserList);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View fb_user_item_view = convertView;
			if(fb_user_item_view == null) {
				fb_user_item_view = getLayoutInflater().inflate(R.layout.user_item_view, parent, false);
			}
			
			// find the user to work with
			UserPojo userPojo = allUserList.get(position);
			
			// fill the view	
			((TextView) fb_user_item_view.findViewById(R.id.tview_user_item_userseqid_id)).setText(String.valueOf(userPojo.getUserSeqId()));
			((TextView) fb_user_item_view.findViewById(R.id.tview_user_item_userid_id)).setText(String.valueOf(userPojo.getUserId()));
			((TextView) fb_user_item_view.findViewById(R.id.tview_user_item_userfirstname_id)).setText(String.valueOf(userPojo.getUserFirstName()));
			((TextView) fb_user_item_view.findViewById(R.id.tview_user_item_userlastname_id)).setText(String.valueOf(userPojo.getUserLastName()));
			return fb_user_item_view;
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.all_users_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
