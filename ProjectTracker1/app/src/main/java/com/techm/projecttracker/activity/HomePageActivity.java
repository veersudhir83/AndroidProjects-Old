package com.techm.projecttracker.activity;

import java.util.ArrayList;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.techm.projecttracker.R;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.TaskPojo;
import com.techm.projecttracker.pojo.UserStoryPojo;

public class HomePageActivity extends ExpandableListActivity {
	private static final int MENU_VIEW_USER_STORY = 0;
	private static final int MENU_EDIT_USER_STORY = 1;
	private ArrayList<UserStoryPojo> parents;
	private int ChildClickStatus=-1;
	private int ParentClickStatus=-1;
	ImageButton imageButton;
	MyExpandableListAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// to show the welcome message
		/*
		 * setContentView(R.layout.activity_home_page); Gson gS = new Gson();
		 * String userObjAsString =
		 * getIntent().getStringExtra(ProjectTrackerConstants
		 * .EXTRA_USER_OBJ_AS_STRING); UserPojo userObj =
		 * gS.fromJson(userObjAsString, UserPojo.class); TextView welcomeMessage
		 * = (TextView) findViewById(R.id.tview_homepage_welcome_id);
		 * welcomeMessage.setText("Welcome " + userObj.getUserFirstName() + " "
		 * + userObj.getUserLastName() + "\n" + userObj.getUserProject() + "  "
		 * + userObj.getUserProjectRole());
		 */

		// to show the user story and tasks
		Resources res = this.getResources();
		Drawable divider = res.getDrawable(R.drawable.line);

		// Set ExpandableListView values
		getExpandableListView().setGroupIndicator(null);
		getExpandableListView().setDivider(divider);
		getExpandableListView().setChildDivider(divider);
		getExpandableListView().setDividerHeight(1);
		registerForContextMenu(getExpandableListView());

		// Creating static data in arraylist
		ArrayList<UserStoryPojo> userStoryList = new ArrayList<UserStoryPojo>();  //buildDummyData();
		userStoryList = getAllUserStoryList();
		

		// Adding ArrayList data to ExpandableListView values
		loadHosts(userStoryList);
	}
	
	private ArrayList<UserStoryPojo> getAllUserStoryList() {
		ArrayList<UserStoryPojo> userStoryList = new ArrayList<UserStoryPojo>();
		ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
		userStoryList = (ArrayList<UserStoryPojo>)fb.getAllUsersStoriesAsList();
		return userStoryList;
	}
	
	

	private void loadHosts(final ArrayList<UserStoryPojo> newParents) {
		if (newParents == null)
			return;
		parents = newParents;

		// Check for ExpandableListAdapter object
		if (this.getExpandableListAdapter() == null) {
			// Create ExpandableListAdapter Object
			mAdapter = new MyExpandableListAdapter();
			
			

			// Set Adapter to ExpandableList Adapter
			this.setListAdapter(mAdapter);
		} else {
			// Refresh ExpandableListView data
			((MyExpandableListAdapter) getExpandableListAdapter())
					.notifyDataSetChanged();
		}
	}
	

	/**
	 * A Custom adapter to create Parent view (Used view_user_story.xml) and Child View(Used view_task.xml).
	 */
	private class MyExpandableListAdapter extends BaseExpandableListAdapter
	{

		private LayoutInflater inflater;

		public MyExpandableListAdapter()
		{
			// Create Layout Inflator
			inflater = LayoutInflater.from(HomePageActivity.this);
		}
		
		// This Function used to inflate parent rows view
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, 
				View convertView, ViewGroup parentView)
		{
			final UserStoryPojo parent = parents.get(groupPosition);
			
			// Inflate grouprow.xml file for parent rows
			convertView = inflater.inflate(R.layout.view_user_story, parentView, false); 
			
			// Get view_user_story.xml file elements and set values
			((TextView) convertView.findViewById(R.id.tview_user_story_id)).setText("  " + parent.getStoryId() + "    ");
			
			String storyTitle = parent.getStoryTitle();
			if(storyTitle.length() > 25) {
				storyTitle = storyTitle.substring(0, 22) + "...";
			}
			((TextView) convertView.findViewById(R.id.tview_user_story_title_id)).setText(storyTitle);
			
			((TextView) convertView.findViewById(R.id.tview_user_story_status_id)).setText(parent.getStoryStatus() + "  ");
			
			return convertView;
		}
		
		// This Function used to inflate child rows view
		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, 
				View convertView, ViewGroup parentView)
		{
			final UserStoryPojo parent = parents.get(groupPosition);
			final TaskPojo child = parent.getTasksList().get(childPosition);
			
			// Inflate childrow.xml file for child rows
			convertView = inflater.inflate(R.layout.view_task, parentView, false);
			
			// Get childrow.xml file elements and set values
			((TextView) convertView.findViewById(R.id.tview_task_id)).setText("      " + child.getTaskId() + "    ");
			String taskTitle = child.getTaskTitle();
			if(taskTitle.length() > 25) {
				taskTitle = taskTitle.substring(0, 22) + "...";
			}
			((TextView) convertView.findViewById(R.id.tview_task_title_id)).setText(taskTitle);
			((TextView) convertView.findViewById(R.id.tview_task_ownername_id)).setText(child.getTaskOwnerName() + "    ");
			
			return convertView;
		}
		
		@Override
		public Object getChild(int groupPosition, int childPosition)
		{
			//Log.i("Childs", groupPosition+"=  getChild =="+childPosition);
			return parents.get(groupPosition).getTasksList().get(childPosition);
		}

		//Call when child row clicked
		@Override
		public long getChildId(int groupPosition, int childPosition)
		{
			/****** When Child row clicked then this function call *******/
			
			//Log.i("Noise", "parent == "+groupPosition+"=  child : =="+childPosition);
			if( ChildClickStatus!=childPosition)
			{
			   ChildClickStatus = childPosition;
			   
			   //Toast.makeText(getApplicationContext(), "Parent :"+groupPosition + " Child :"+childPosition ,Toast.LENGTH_LONG).show();
			   UserStoryPojo parent = parents.get(groupPosition);
			   TaskPojo child = parent.getTasksList().get(childPosition);
			   //Toast.makeText(getApplicationContext(), "TaskID :"+ child.getTaskId() ,Toast.LENGTH_LONG).show();
			}  
			
			return childPosition;
		}

		@Override
		public int getChildrenCount(int groupPosition)
		{
			int size=0;
			if(parents.get(groupPosition).getTasksList()!=null)
				size = parents.get(groupPosition).getTasksList().size();
			return size;
		}
		
		@Override
		public Object getGroup(int groupPosition)
		{
			//Log.i("Parent", groupPosition+"=  getGroup ");
			
			return parents.get(groupPosition);
		}

		@Override
		public int getGroupCount()
		{
			return parents.size();
		}

		//Call when parent row clicked
		@Override
		public long getGroupId(int groupPosition)
		{
			//Log.i("Parent", groupPosition+"=  getGroupId "+ParentClickStatus);
			
			if(groupPosition==1 && ParentClickStatus!=groupPosition){
				
				//Alert to user
				//Toast.makeText(getApplicationContext(), "Parent :"+groupPosition ,Toast.LENGTH_LONG).show();
			}
			
			ParentClickStatus=groupPosition;
			if(ParentClickStatus==0)
				ParentClickStatus=-1;
			
			return groupPosition;
		}

		@Override
		public void notifyDataSetChanged()
		{
			// Refresh List rows
			super.notifyDataSetChanged();
		}

		@Override
		public boolean isEmpty()
		{
			return ((parents == null) || parents.isEmpty());
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition)
		{
			return true;
		}

		@Override
		public boolean hasStableIds()
		{
			return true;
		}

		@Override
		public boolean areAllItemsEnabled()
		{
			return true;
		}
	}
	

	@Override 
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
    {
            super.onCreateContextMenu(menu, v, menuInfo);
            
            ExpandableListView.ExpandableListContextMenuInfo info =
            		(ExpandableListView.ExpandableListContextMenuInfo) menuInfo;

            int type =
            		ExpandableListView.getPackedPositionType(info.packedPosition);
            int group =
            		ExpandableListView.getPackedPositionGroup(info.packedPosition);
            int child =
            		ExpandableListView.getPackedPositionChild(info.packedPosition);
            // Only create a context menu for child items
            if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            	Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "Inside onCreateContextMenu:::GROUP::" + group + "::CHILD::" + child);

            	menu.setHeaderTitle("Select The Action");
            	menu.add(0, MENU_VIEW_USER_STORY, 0, "View Task");
            	menu.add(0, MENU_EDIT_USER_STORY, 0, "Edit Task");
            } else if (type == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            	Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "Inside onCreateContextMenu:::GROUP::" + group);

            	menu.setHeaderTitle("Select The Action");
            	menu.add(0, MENU_VIEW_USER_STORY, 0, "View User Story");
            	menu.add(0, MENU_EDIT_USER_STORY, 0, "Edit User Story");
            }
     } 

	@Override  
	public boolean onContextItemSelected(MenuItem item)
	{  
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "onContextItemSelected");

		ExpandableListContextMenuInfo info = 
				(ExpandableListContextMenuInfo) item.getMenuInfo();

		int groupPos = 0, childPos = 0;

		int type = ExpandableListView.getPackedPositionType(info.packedPosition);
		if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) 
		{
			groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
			childPos = ExpandableListView.getPackedPositionChild(info.packedPosition);
			Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "groupPos::" + groupPos + "::childPos::" + childPos);
		} else if (type == ExpandableListView.PACKED_POSITION_TYPE_GROUP) 
		{
			groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
			Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "groupPos::" + groupPos);
		}

		try
		{
			if(item.getTitle()=="View User Story")
			{
				viewUserStory(parents.get(groupPos).getStoryId());
			}  
			else if(item.getTitle()=="Edit User Story")
			{
				editUserStory(parents.get(groupPos).getStoryId());
			} else if(item.getTitle()=="View Task")
			{
				viewTask(parents.get(groupPos).getTasksList().get(childPos).getTaskId());
			}  
			else if(item.getTitle()=="Edit Task")
			{
				editTask(parents.get(groupPos).getTasksList().get(childPos).getTaskId());
			}
			else 
			{
				return false;
			}  
			return true;  
		}
		catch(Exception e)
		{
			return true;
		}	
	}  
	
	 private void viewUserStory(String userStoryId){
		 //Toast.makeText(this, "viewUserStory called for " + userStoryId, Toast.LENGTH_SHORT).show();
		 Intent intent = new Intent();
		 intent.setClass(this, UserStoryViewActivity.class);
		 intent.putExtra(ProjectTrackerConstants.EXTRA_USER_STORY_ID, userStoryId.trim());
		 startActivity(intent);
	 }
	 
	 private void editUserStory(String userStoryId){
		 //Toast.makeText(this, "viewUserStory called for" + userStoryId, Toast.LENGTH_SHORT).show();
		 Intent intent = new Intent();
		 intent.setClass(this, UserStoryEditActivity.class);
		 intent.putExtra(ProjectTrackerConstants.EXTRA_USER_STORY_ID, userStoryId.trim());
		 startActivity(intent);
	 }
	 
	 private void viewTask(String taskId){
		 //Toast.makeText(this, "viewTask called for " + taskId, Toast.LENGTH_SHORT).show();
		 Intent intent = new Intent();
		 intent.setClass(this, TaskViewActivity.class);
		 intent.putExtra(ProjectTrackerConstants.EXTRA_TASK_ID, taskId.trim());
		 startActivity(intent);
	 }
	 
	 private void editTask(String taskId){
		 //Toast.makeText(this, "editTask called for" + taskId, Toast.LENGTH_SHORT).show();
		 Intent intent = new Intent();
		 intent.setClass(this, TaskEditActivity.class);
		 intent.putExtra(ProjectTrackerConstants.EXTRA_TASK_ID, taskId.trim());
		 startActivity(intent);
	 }
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_page, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.menuitem_action_profile_id:
	            showUserProfile();
	            return true;
	        case R.id.menuitem_action_settings_id:
	            //showAppSettings();
	            return true;
	        case R.id.menuitem_show_all_users_id:
	            showAllUsers();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	/**
	 * Method to show all users
	 */
	private void showAllUsers() {
		Intent intent = new Intent();
		intent.setClass(this, AllUserListActivity.class);
		startActivity(intent);
	}

	/**
	 * Method to open User profile activity
	 */
	private void showUserProfile() {
		Intent intent = new Intent();
		intent.setClass(this, UserProfileActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Method to insert dummy data
	 * @return
	 */
	private ArrayList<UserStoryPojo> buildDummyData() {
		// Creating ArrayList of type parent class to store parent class objects
		final ArrayList<UserStoryPojo> list = new ArrayList<UserStoryPojo>();
		for (int i = 1; i <= 2; i++) {
			// Create parent class object
			final UserStoryPojo parent = new UserStoryPojo();

			// Set values in parent class object
			if (i == 1) {
				parent.setStoryId("" + i);
				parent.setStoryTitle("Parent 0");
				parent.setStoryOwnerName("st0_owner");
				parent.setStoryPoints("5");
				parent.setStoryStatus("Defined");
				parent.setTasksList(new ArrayList<TaskPojo>());

				// Create Child class object
				final TaskPojo child0 = new TaskPojo();
				child0.setTaskId("" + i);
				child0.setTaskTitle("Child 0.0");
				child0.setTaskOwnerName("Child 0 owner");

				// Add Child class object to parent class object
				parent.getTasksList().add(child0);
			} else if (i == 2) {
				parent.setStoryId("" + i);
				parent.setStoryTitle("Parent 1");
				parent.setStoryOwnerName("st1_owner");
				parent.setStoryPoints("3");
				parent.setStoryStatus("In Progress");
				parent.setTasksList(new ArrayList<TaskPojo>());

				// Create Child class object
				final TaskPojo child0 = new TaskPojo();
				child0.setTaskId("" + i);
				child0.setTaskTitle("Child 1.0");
				child0.setTaskOwnerName("Child 1.0 owner");
				
				final TaskPojo child1 = new TaskPojo();
				child1.setTaskId("" + i);
				child1.setTaskTitle("Child 1.1");
				child1.setTaskOwnerName("Child 1.1 owner");

				// Add Child class object to parent class object
				parent.getTasksList().add(child0);
				parent.getTasksList().add(child1);
			} 
			// Adding Parent class object to ArrayList
			list.add(parent);
		}
		return list;
	}
}
