package com.techm.projecttracker.activity;

import java.util.Locale;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.techm.projecttracker.R;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.UserPojo;

public class MainActivity extends Activity {
	
	private EditText etUserId;
	private EditText etUserPassword;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		etUserId = ((EditText)findViewById(R.id.eText_login_username_id));
		etUserPassword = ((EditText)findViewById(R.id.eText_login_password_id));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Method to reset the values in login details
	 * @param view
	 */
	public void resetLoginFields(View view) {
		
		etUserId.setText("");
		etUserPassword.setText("");
		etUserId.requestFocus();
		/*etUserId.post(new Runnable() {
		    public void run() {
		    	etUserId.requestFocusFromTouch();
		        InputMethodManager lManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
		        lManager.showSoftInput(etUserId, 0);
		    }
		});*/
	}
	
	/**
	 * Method to validate the User Credentials from projectTrackerDb.PT_USER table
	 * @param view
	 */
	public void validateLoginInformation(View view) {
		ProjectTrackerDatabase db = new ProjectTrackerDatabase(getApplicationContext());
		String fullName = "";
		UserPojo userObj = null; 
		if(etUserId.getText().toString().trim().length() > 0 && etUserPassword.getText().toString().trim().length() > 0) {
			userObj = db.validateUser(etUserId.getText().toString(), etUserPassword.getText().toString());
			if (null != userObj) {
				//Toast.makeText(getApplicationContext(), "Validated Successfully !!", Toast.LENGTH_LONG).show();
				Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "Validated User");
				
				// TO convert the user object to string representation using GSON library
				Gson gS = new Gson();
				String userObjAsString = gS.toJson(userObj);
				
				// Set user object to preferences
				SharedPreferences settings = getSharedPreferences(ProjectTrackerConstants.PREFS_NAME, MODE_PRIVATE);
			    SharedPreferences.Editor editor = settings.edit();
			    editor.putString(ProjectTrackerConstants.EXTRA_USER_OBJ_AS_STRING, userObjAsString);

			    // Commit the edits to preferences file!
			    editor.commit();
				
			    // to call the next activity (Home Page)
				Intent intent = new Intent();
				intent.putExtra(ProjectTrackerConstants.EXTRA_LOGIN_USER_NAME, fullName.toUpperCase(Locale.ENGLISH));
				intent.setClass(this, HomePageActivity.class);
				//intent.setClass(this, DeveloperHomePage.class);
				startActivity(intent);
			} else {
				Toast.makeText(getApplicationContext(), "Invalid Credentials !!", Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(getApplicationContext(), "Please input your User Name and Password !!", Toast.LENGTH_LONG).show();
		}
	}
	
	/**
	 * Method to open SMS window to reset password
	 */
	public void forgotPassword(View view) {
		// code to open Message window with default number to send SMS
		
		Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("address", ProjectTrackerConstants.ADMIN_LOST_PASSWORD_NUMBER);
		smsIntent.putExtra("sms_body","Body of Message");
		startActivity(smsIntent);
		
		
		// Code to directly send SMS and show the message sending feedback as a Toast
		/*String phoneNumber = ProjectTrackerConstants.ADMIN_LOST_PASSWORD_NUMBER;
	    String message = "Body of Message";
	    sendSMS(phoneNumber, message);
	    */

	}
	
	private void sendSMS(String phoneNumber, String message)
    {        
		//---sends an SMS message to another device---
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
            new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
            new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off", 
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered", 
                                Toast.LENGTH_SHORT).show();
                        break;                        
                }
            }
        }, new IntentFilter(DELIVERED));        

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);        
    }
	
	
	
}
