package com.techm.projecttracker.activity;

import java.util.ArrayList;
import java.util.List;

import com.techm.projecttracker.R;
import com.techm.projecttracker.R.layout;
import com.techm.projecttracker.R.menu;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.UserStoryPojo;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class UserStoryEditActivity extends Activity {
	
	UserStoryPojo userStoryPojo = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_story_edit);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Bundle bundle = getIntent().getExtras();
		String userStoryId = bundle.getString(ProjectTrackerConstants.EXTRA_USER_STORY_ID);

        if(userStoryId != null && userStoryId.trim().length() > 0)
        {
        	getUserStoryDetails(userStoryId.trim());
        	
        	if(userStoryPojo != null) {
        		
	            ((TextView) findViewById(R.id.tview_user_story_edit_storyid_value_id)).setText(userStoryPojo.getStoryId());
	            ((TextView) findViewById(R.id.et_user_story_edit_storytitle_value_id)).setText(userStoryPojo.getStoryTitle());
	            ((TextView) findViewById(R.id.et_user_story_edit_storydescription_value_id)).setText(userStoryPojo.getStoryDescription());
	            ((TextView) findViewById(R.id.tview_user_story_edit_storyowner_value_id)).setText(userStoryPojo.getStoryOwnerName());
	            
	            // to set the drop down with user story points value 
	            final Spinner spinnerPoints=(Spinner) findViewById(R.id.spinner_edit_user_story_points_id);
	            ArrayAdapter<String> adapterPoints= new ArrayAdapter<String>(this,
	                                            android.R.layout.simple_list_item_1,ProjectTrackerConstants.STORY_POINTS_LIST);
	            adapterPoints.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	            Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "list.indexOf(userStoryPojo.getStoryPoints()) ::" + ProjectTrackerConstants.STORY_POINTS_LIST.indexOf(userStoryPojo.getStoryPoints()));
	            spinnerPoints.setAdapter(adapterPoints);
	            spinnerPoints.setSelection(ProjectTrackerConstants.STORY_POINTS_LIST.indexOf(userStoryPojo.getStoryPoints()));
	            
	            // to set the drop down with user story status value
	            final Spinner spinnerStatus=(Spinner) findViewById(R.id.spinner_edit_user_story_status_id);
	            ArrayAdapter<String> adapterStatus= new ArrayAdapter<String>(this,
	                                            android.R.layout.simple_list_item_1,ProjectTrackerConstants.STORY_STATUS_LIST);
	            adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	            Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "list.indexOf(userStoryPojo.getStoryStatus()) ::" + ProjectTrackerConstants.STORY_STATUS_LIST.indexOf(userStoryPojo.getStoryStatus()));
	            spinnerStatus.setAdapter(adapterStatus);
	            spinnerStatus.setSelection(ProjectTrackerConstants.STORY_STATUS_LIST.indexOf(userStoryPojo.getStoryStatus()));
        	}
        }
	}
	
	/**
	 * To return User story details
	 * @return
	 */
	private void getUserStoryDetails(String userStoryId) {
		ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
		userStoryPojo = (UserStoryPojo) fb.getUserStoryDetails(userStoryId);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_story_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Toast.makeText(getApplicationContext(), "Clicked Up on Home !!", Toast.LENGTH_LONG).show();
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Method to save the modified user story details
	 * @param view
	 */
	public void saveUserStoryEdit(View view) {
		String storyId = ((TextView) findViewById(R.id.tview_user_story_edit_storyid_value_id)).getText().toString();
		String title = ((TextView) findViewById(R.id.et_user_story_edit_storytitle_value_id)).getText().toString();
        String description = ((TextView) findViewById(R.id.et_user_story_edit_storydescription_value_id)).getText().toString();
        //String owner = ((TextView) findViewById(R.id.tview_user_story_edit_storyowner_value_id)).getText().toString();
        String points = ((Spinner)findViewById(R.id.spinner_edit_user_story_points_id)).getSelectedItem().toString();
        String status = ((Spinner)findViewById(R.id.spinner_edit_user_story_status_id)).getSelectedItem().toString();
        ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
        if(fb.saveUserStoryEdit(storyId, title, description, Integer.parseInt(points), status) == 1) {
        	Toast.makeText(getApplicationContext(), "User Story Updated !!", Toast.LENGTH_LONG).show();
        } else {
        	Toast.makeText(getApplicationContext(), "User Story Update Failed !!", Toast.LENGTH_LONG).show();
        }
	}
	
}
