package com.techm.projecttracker.pojo;

public class UserPojo {
	private String userId;
	private String userFirstName;
	private String userLastName;
	private String userActiveInd;
	private String userProject;
	private String userProjectRole;
	private String userEmailAddress;
	private String userPhoneNumber;
	private int userSeqId;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserActiveInd() {
		return userActiveInd;
	}
	public void setUserActiveInd(String userActiveInd) {
		this.userActiveInd = userActiveInd;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserProject() {
		return userProject;
	}
	public void setUserProject(String userProject) {
		this.userProject = userProject;
	}
	public String getUserProjectRole() {
		return userProjectRole;
	}
	public void setUserProjectRole(String userProjectRole) {
		this.userProjectRole = userProjectRole;
	}
	public int getUserSeqId() {
		return userSeqId;
	}
	public void setUserSeqId(int userSeqId) {
		this.userSeqId = userSeqId;
	}
	public String getUserEmailAddress() {
		return userEmailAddress;
	}
	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
}
