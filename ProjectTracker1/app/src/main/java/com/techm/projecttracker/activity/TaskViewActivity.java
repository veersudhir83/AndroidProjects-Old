package com.techm.projecttracker.activity;

import com.techm.projecttracker.R;
import com.techm.projecttracker.R.layout;
import com.techm.projecttracker.R.menu;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.TaskPojo;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class TaskViewActivity extends Activity {
	
	TaskPojo taskPojo = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_view);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Bundle bundle = getIntent().getExtras();
		String taskId = bundle.getString(ProjectTrackerConstants.EXTRA_TASK_ID);
		
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "taskId from Home:" + taskId);
		
		if(taskId != null && taskId.trim().length() > 0)
        {
        	getTaskDetails(taskId.trim());
        	
        	if(taskPojo != null) {
        		((TextView) findViewById(R.id.tview_task_view_taskid_value_id)).setText(taskPojo.getTaskId());
	            ((TextView) findViewById(R.id.tview_task_view_tasktitle_value_id)).setText(taskPojo.getTaskTitle());
	            ((TextView) findViewById(R.id.tview_task_view_taskdescription_value_id)).setText(taskPojo.getTaskDescription());
	            ((TextView) findViewById(R.id.tview_task_view_taskowner_value_id)).setText(taskPojo.getTaskOwnerName());
	            ((TextView) findViewById(R.id.tview_task_view_taskesthrs_value_id)).setText(taskPojo.getTaskEstimateHrs());
	            ((TextView) findViewById(R.id.tview_task_view_taskremhrs_value_id)).setText(taskPojo.getTaskRemainingHrs());
        	}
        }
	}
	
	/**
	 * To return Task details
	 * @return
	 */
	private void getTaskDetails(String taskId) {
		ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
		taskPojo = (TaskPojo) fb.getTaskDetails(taskId);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
