package com.techm.projecttracker.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.pojo.TaskPojo;
import com.techm.projecttracker.pojo.UserPojo;
import com.techm.projecttracker.pojo.UserStoryPojo;

public class ProjectTrackerDatabase {
	private SQLiteDatabase database;
	private MySqlLiteHelper dbHelper;
	
	public ProjectTrackerDatabase (Context context) {
		dbHelper = new MySqlLiteHelper(context);
	}
	
	public void open() {
		try {
			database = dbHelper.getWritableDatabase();	
		} catch (Exception e) {
			Log.d("error", e.toString());
		}
	}
	
	public void close() {
		database.close();
		dbHelper.close();
	}
	
	public void insert(String fbEntered) {
		open();
		//database.execSQL("insert into feedback (feedback) values('" + fbEntered + "')");
	}
	
	/**
	 * Validate the user based on credentials
	 * @param userNameInput
	 * @param userPasswordInput
	 * @return boolean
	 */
	public UserPojo validateUser(String userNameInput, String userPasswordInput) {
		UserPojo userObj = null;
		open();
		
		// CODE TO FETCH THE COUNT OF RECORDS IN THE PT_USER TABLE - UN COMMENT IF NEED TO DEBUG
		/*String countQuery = "SELECT COUNT(1) FROM PT_USER WHERE USER_ACTIVE_IND = 1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "countQuery:" + countQuery);
		Cursor cCount = database.rawQuery(countQuery, null);
		if(cCount.moveToFirst()) {
			do {
				Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "Inside cCountCursor");
				Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "count of records::" + cCount.getInt(0));
				cCount.moveToNext();
			} while(cCount.isAfterLast() == false);
		} */
		
		// START: CODE TO VALIDATE USER AND FETCH DETAILS
		String userValidationQuery = "SELECT USER_FIRST_NAME, USER_LAST_NAME, USER_EMAIL_ADDRESS , USER_MOBILE_NUMBER FROM PT_USER WHERE UPPER(USER_ID) = UPPER(?) AND USER_PASSWORD = ? AND ACTIVE_IND = 1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userValidationQuery:" + userValidationQuery);
		Cursor cUserCursor = database.rawQuery(userValidationQuery, new String[]{userNameInput, userPasswordInput});
		for(cUserCursor.moveToFirst(); !(cUserCursor.isAfterLast()); cUserCursor.moveToNext()){
			userObj = new UserPojo();
			Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "Inside cUserCursor");
			userObj.setUserId(userNameInput);
			userObj.setUserFirstName(cUserCursor.getString(cUserCursor.getColumnIndex("USER_FIRST_NAME")));
			userObj.setUserLastName(cUserCursor.getString(cUserCursor.getColumnIndex("USER_LAST_NAME")));
			userObj.setUserEmailAddress(cUserCursor.getString(cUserCursor.getColumnIndex("USER_EMAIL_ADDRESS")));
			userObj.setUserPhoneNumber(cUserCursor.getString(cUserCursor.getColumnIndex("USER_MOBILE_NUMBER")));
		}
		cUserCursor.close();
		if(userObj != null) {
			// START: CODE TO FETCH USER PROJECT NAME
			String userProjectQuery = "SELECT PROJECT_NAME FROM PT_PROJECT WHERE PROJECT_SEQ_ID IN (SELECT PROJECT_SEQ_ID FROM PT_USER_PROJECT_ROLE WHERE USER_SEQ_ID IN (SELECT USER_SEQ_ID FROM PT_USER WHERE UPPER(USER_ID) = UPPER(?) AND ACTIVE_IND =1) AND ACTIVE_IND = 1) AND ACTIVE_IND = 1";
			Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userProjectQuery:" + userProjectQuery);
			Cursor cProjectCursor = database.rawQuery(userProjectQuery, new String[]{userNameInput});
			for(cProjectCursor.moveToFirst(); !(cProjectCursor.isAfterLast()); cProjectCursor.moveToNext()){
				Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "Inside cProjectCursor");
				userObj.setUserProject(cProjectCursor.getString(cProjectCursor.getColumnIndex("PROJECT_NAME")));
			}
			cProjectCursor.close();
			
			// START: CODE TO FETCH USER ROLE NAME
			String userRoleQuery = "SELECT ROLE_NAME FROM PT_ROLE WHERE ROLE_SEQ_ID IN (SELECT ROLE_SEQ_ID FROM PT_USER_PROJECT_ROLE WHERE USER_SEQ_ID IN (SELECT USER_SEQ_ID FROM PT_USER WHERE UPPER(USER_ID) = UPPER(?) AND ACTIVE_IND =1) AND ACTIVE_IND = 1) AND ACTIVE_IND = 1";
			Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userRoleQuery:" + userRoleQuery);
			Cursor cRoleCursor = database.rawQuery(userRoleQuery, new String[]{userNameInput});
			for(cRoleCursor.moveToFirst(); !(cRoleCursor.isAfterLast()); cRoleCursor.moveToNext()){
				Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "Inside cRoleCursor");
				userObj.setUserProjectRole(cRoleCursor.getString(cRoleCursor.getColumnIndex("ROLE_NAME")));
			}
			cRoleCursor.close();
		}
		return userObj;
	}
	
	/**
	 * Return the list of available active users as a list of user objects
	 * @return List
	 */
	public List<UserPojo> getAllUsersAsList() {
		open();
		String userlistQuery = "SELECT USER_SEQ_ID, USER_ID, USER_FIRST_NAME, USER_LAST_NAME FROM PT_USER WHERE ACTIVE_IND=1";
		List<UserPojo> userList = new ArrayList<UserPojo>();
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userlistQuery:" + userlistQuery);
		Cursor c = database.rawQuery(userlistQuery, null);
		if(c.moveToFirst()) {
			do {
				UserPojo userPojo = new UserPojo();
				userPojo.setUserSeqId(c.getInt(c.getColumnIndex("USER_SEQ_ID")));
				userPojo.setUserId("  "+c.getString(c.getColumnIndex("USER_ID")));
				userPojo.setUserFirstName(c.getString(c.getColumnIndex("USER_FIRST_NAME")));
				userPojo.setUserLastName("  " + c.getString(c.getColumnIndex("USER_LAST_NAME")));
				userList.add(userPojo);
				c.moveToNext();
			} while(c.isAfterLast() == false);
		} 
		c.close();
		return userList;
	}
	
	/**
	 * Return the list of available user stories as a list of user story objects
	 * @return void
	 */
	public List<UserStoryPojo> getAllUsersStoriesAsList() {
		open();
		String userStoryListQuery = "SELECT USER_STORY_SEQ_ID, USER_STORY_ID, USER_STORY_TITLE, USER_STORY_DESCRIPTION, USER_STORY_OWNER_NAME, USER_STORY_POINTS, USER_STORY_STATUS FROM PT_USER_STORY WHERE ACTIVE_IND=1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userStoryListQuery:" + userStoryListQuery);
		String taskListQuery = "SELECT TASK_ID, TASK_TITLE, TASK_DESCRIPTION, TASK_ESTIMATE_HRS, TASK_REMAINING_HRS, TASK_OWNER_NAME FROM PT_TASK WHERE USER_STORY_ID = ? AND ACTIVE_IND=1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "taskListQuery:" + taskListQuery);
		List<UserStoryPojo> userStoryList = new ArrayList<UserStoryPojo>();
		Cursor cursorUserStory = database.rawQuery(userStoryListQuery, null);
		if(cursorUserStory.moveToFirst()) {
			do {
				UserStoryPojo userStoryPojo = new UserStoryPojo();
				userStoryPojo.setStoryId(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_ID")));
				userStoryPojo.setStoryTitle(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_TITLE")));
				userStoryPojo.setStoryDescription(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_DESCRIPTION")));
				userStoryPojo.setStoryOwnerName(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_OWNER_NAME")));
				userStoryPojo.setStoryPoints(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_POINTS")));
				userStoryPojo.setStoryStatus(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_STATUS")));
				userStoryPojo.setTasksList(new ArrayList<TaskPojo>());

				// code to fetch all child tasks for selected user story
				Cursor cursorTask = database.rawQuery(taskListQuery, new String[]{cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_ID"))});
				if(cursorTask.moveToFirst()) {
					do {
						TaskPojo taskPojo = new TaskPojo();
						taskPojo.setTaskId(cursorTask.getString(cursorTask.getColumnIndex("TASK_ID")));
						taskPojo.setTaskTitle(cursorTask.getString(cursorTask.getColumnIndex("TASK_TITLE")));
						taskPojo.setTaskDescription(cursorTask.getString(cursorTask.getColumnIndex("TASK_DESCRIPTION")));
						taskPojo.setTaskEstimateHrs(cursorTask.getString(cursorTask.getColumnIndex("TASK_ESTIMATE_HRS")));
						taskPojo.setTaskRemainingHrs(cursorTask.getString(cursorTask.getColumnIndex("TASK_REMAINING_HRS")));
						taskPojo.setTaskOwnerName(cursorTask.getString(cursorTask.getColumnIndex("TASK_OWNER_NAME")));
						
						//add the task pojo to existing task children on the parent
						userStoryPojo.getTasksList().add(taskPojo);
						
						cursorTask.moveToNext();
					} while(cursorTask.isAfterLast() == false);
				}
				userStoryList.add(userStoryPojo);
				cursorUserStory.moveToNext();
			} while(cursorUserStory.isAfterLast() == false);
		} 
		cursorUserStory.close();
		return userStoryList;
	}
	
	/**
	 * Return the list of available user stories as a list of user story objects
	 * @return void
	 */
	public List<UserStoryPojo> getAllUsersStoriesInfo() {
		open();
		String userStoryListQuery = "SELECT USER_STORY_SEQ_ID, USER_STORY_ID, USER_STORY_TITLE, USER_STORY_OWNER_NAME, USER_STORY_POINTS, USER_STORY_STATUS FROM PT_USER_STORY WHERE ACTIVE_IND=1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userStoryListQuery:" + userStoryListQuery);
		List<UserStoryPojo> userStoryList = new ArrayList<UserStoryPojo>();
		Cursor cursorUserStory = database.rawQuery(userStoryListQuery, null);
		if(cursorUserStory.moveToFirst()) {
			do {
				UserStoryPojo userStoryPojo = new UserStoryPojo();
				userStoryPojo.setStoryId(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_ID")));
				userStoryPojo.setStoryTitle(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_TITLE")));
				userStoryPojo.setStoryOwnerName(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_OWNER_NAME")));
				userStoryPojo.setStoryPoints(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_POINTS")));
				userStoryPojo.setStoryStatus(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_STATUS")));
				userStoryList.add(userStoryPojo);
				cursorUserStory.moveToNext();
			} while(cursorUserStory.isAfterLast() == false);
		} 
		cursorUserStory.close();
		return userStoryList;
	}
	
	/**
	 * Method to fetch details of the User story
	 * @param userStoryId
	 * @return
	 */
	public UserStoryPojo getUserStoryDetails(String userStoryId) {
		open();
		UserStoryPojo userStoryPojo = null;
		String userStoryDetailsQuery = "SELECT USER_STORY_ID, USER_STORY_TITLE, USER_STORY_DESCRIPTION, USER_STORY_OWNER_NAME, USER_STORY_POINTS, USER_STORY_STATUS FROM PT_USER_STORY WHERE USER_STORY_ID = ? AND ACTIVE_IND=1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "userStoryDetailsQuery:" + userStoryDetailsQuery);
		Cursor cursorUserStory = database.rawQuery(userStoryDetailsQuery, new String[] {userStoryId});
		if(cursorUserStory.moveToFirst()) {
			do {
				userStoryPojo = new UserStoryPojo();
				userStoryPojo.setStoryId(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_ID")));
				userStoryPojo.setStoryTitle(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_TITLE")));
				userStoryPojo.setStoryDescription(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_DESCRIPTION")));
				userStoryPojo.setStoryOwnerName(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_OWNER_NAME")));
				userStoryPojo.setStoryPoints(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_POINTS")));
				userStoryPojo.setStoryStatus(cursorUserStory.getString(cursorUserStory.getColumnIndex("USER_STORY_STATUS")));
				cursorUserStory.moveToNext();
			} while(cursorUserStory.isAfterLast() == false);
		} 
		cursorUserStory.close();
		return userStoryPojo;
	}

	/**
	 * Method to update the user story details
	 * @param storyId
	 * @param title
	 * @param description
	 * @param parseInt
	 * @param status
	 */
	public int saveUserStoryEdit(String storyId, String title,
			String description, int points, String status) {
		open();
		ContentValues values = new ContentValues();
		values.put("USER_STORY_TITLE", title);
		values.put("USER_STORY_DESCRIPTION", description);
		values.put("USER_STORY_POINTS", points);
		values.put("USER_STORY_STATUS", status);
		int updateStatus = database.update("PT_USER_STORY", values, "USER_STORY_ID=?", new String[] {storyId});
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "udpateStatus::" + updateStatus);  
		return updateStatus;
		
	}

	/**
	 * Method to fetch Task Details
	 * @param taskId
	 * @return
	 */
	public TaskPojo getTaskDetails(String taskId) {
		open();
		TaskPojo taskPojo = null;
		String taskDetailsQuery = "SELECT TASK_ID, TASK_TITLE, TASK_DESCRIPTION, TASK_OWNER_NAME, TASK_ESTIMATE_HRS, TASK_REMAINING_HRS FROM PT_TASK WHERE TASK_ID = ? AND ACTIVE_IND=1";
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER , "taskDetailsQuery:" + taskDetailsQuery);
		Cursor taskCursor = database.rawQuery(taskDetailsQuery, new String[] {taskId});
		if(taskCursor.moveToFirst()) {
			do {
				taskPojo = new TaskPojo();
				taskPojo.setTaskId(taskCursor.getString(taskCursor.getColumnIndex("TASK_ID")));
				taskPojo.setTaskTitle(taskCursor.getString(taskCursor.getColumnIndex("TASK_TITLE")));
				taskPojo.setTaskDescription(taskCursor.getString(taskCursor.getColumnIndex("TASK_DESCRIPTION")));
				taskPojo.setTaskOwnerName(taskCursor.getString(taskCursor.getColumnIndex("TASK_OWNER_NAME")));
				taskPojo.setTaskEstimateHrs(taskCursor.getString(taskCursor.getColumnIndex("TASK_ESTIMATE_HRS")));
				taskPojo.setTaskRemainingHrs(taskCursor.getString(taskCursor.getColumnIndex("TASK_REMAINING_HRS")));
				taskCursor.moveToNext();
			} while(taskCursor.isAfterLast() == false);
		} 
		taskCursor.close();
		return taskPojo;
	}

	/**
	 * Method to update Task Details
	 * @param taskId
	 * @param title
	 * @param description
	 * @param estHrs
	 * @param remHrs
	 */
	public int saveTaskEdit(String taskId, String title, String description,
			String estHrs, String remHrs) {
		open();
		ContentValues values = new ContentValues();
		values.put("TASK_TITLE", title);
		values.put("TASK_DESCRIPTION", description);
		values.put("TASK_ESTIMATE_HRS", estHrs);
		values.put("TASK_REMAINING_HRS", remHrs);
		int updateStatus = database.update("PT_TASK", values, "TASK_ID=?", new String[] {taskId});
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "udpateStatus::" + updateStatus);  
		return updateStatus;
	}
}
