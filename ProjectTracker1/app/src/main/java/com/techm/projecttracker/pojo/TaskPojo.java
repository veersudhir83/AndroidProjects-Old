package com.techm.projecttracker.pojo;

public class TaskPojo {
	private String taskId;
	private String taskTitle;
	private String taskDescription;
	private String taskOwnerId;
	private String taskOwnerName;
	private String taskStatus;
	private String taskEstimateHrs;
	private String taskRemainingHrs;
	
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskTitle() {
		return taskTitle;
	}
	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public String getTaskOwnerId() {
		return taskOwnerId;
	}
	public void setTaskOwnerId(String taskOwnerId) {
		this.taskOwnerId = taskOwnerId;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	public String getTaskEstimateHrs() {
		return taskEstimateHrs;
	}
	public void setTaskEstimateHrs(String taskEstimateHrs) {
		this.taskEstimateHrs = taskEstimateHrs;
	}
	public String getTaskRemainingHrs() {
		return taskRemainingHrs;
	}
	public void setTaskRemainingHrs(String taskRemainingHrs) {
		this.taskRemainingHrs = taskRemainingHrs;
	}
	public String getTaskOwnerName() {
		return taskOwnerName;
	}
	public void setTaskOwnerName(String taskOwnerName) {
		this.taskOwnerName = taskOwnerName;
	}
	
}
