package com.techm.projecttracker.activity;

import com.techm.projecttracker.R;
import com.techm.projecttracker.R.layout;
import com.techm.projecttracker.R.menu;
import com.techm.projecttracker.constants.ProjectTrackerConstants;
import com.techm.projecttracker.database.ProjectTrackerDatabase;
import com.techm.projecttracker.pojo.TaskPojo;
import com.techm.projecttracker.pojo.UserStoryPojo;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class TaskEditActivity extends Activity {

	TaskPojo taskPojo = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_edit);
		// Show the Up button in the action bar.
		setupActionBar();
		
		
		
		Bundle bundle = getIntent().getExtras();
		String taskId = bundle.getString(ProjectTrackerConstants.EXTRA_TASK_ID);
		
		Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "taskId from Home:" + taskId);
		
		if(taskId != null && taskId.trim().length() > 0)
        {
        	getTaskDetails(taskId.trim());
        	
        	if(taskPojo != null) {
        		((TextView) findViewById(R.id.tview_task_edit_taskid_value_id)).setText(taskPojo.getTaskId());
	            ((TextView) findViewById(R.id.et_task_edit_tasktitle_value_id)).setText(taskPojo.getTaskTitle());
	            ((TextView) findViewById(R.id.tview_task_edit_taskdescription_value_id)).setText(taskPojo.getTaskDescription());
	            ((TextView) findViewById(R.id.tview_task_edit_taskowner_value_id)).setText(taskPojo.getTaskOwnerName());
	            
	            // to set the drop down with estimated hours value 
	            final Spinner spinnerEstHrs=(Spinner) findViewById(R.id.spinner_edit_task_est_hrs_id);
	            ArrayAdapter<String> adapterEstHrs= new ArrayAdapter<String>(this,
	                                            android.R.layout.simple_list_item_1,ProjectTrackerConstants.TASK_EST_HOURS_LIST);
	            adapterEstHrs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	            Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "list.indexOf(taskPojo.getTaskEstimateHrs()) ::" + ProjectTrackerConstants.TASK_EST_HOURS_LIST.indexOf(taskPojo.getTaskEstimateHrs()));
	            spinnerEstHrs.setAdapter(adapterEstHrs);
	            spinnerEstHrs.setSelection(ProjectTrackerConstants.TASK_EST_HOURS_LIST.indexOf(taskPojo.getTaskEstimateHrs()));
	            
	            // to set the drop down with remaining hours value 
	            final Spinner spinnerRemHrs=(Spinner) findViewById(R.id.spinner_edit_task_rem_hrs_id);
	            ArrayAdapter<String> adapterRemHrs= new ArrayAdapter<String>(this,
	                                            android.R.layout.simple_list_item_1,ProjectTrackerConstants.TASK_EST_HOURS_LIST);
	            adapterRemHrs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	            Log.d(ProjectTrackerConstants.APP_DEBUG_TRACKER, "list.indexOf(taskPojo.getTaskRemainingHrs()) ::" + ProjectTrackerConstants.TASK_EST_HOURS_LIST.indexOf(taskPojo.getTaskRemainingHrs()));
	            spinnerRemHrs.setAdapter(adapterRemHrs);
	            spinnerRemHrs.setSelection(ProjectTrackerConstants.TASK_EST_HOURS_LIST.indexOf(taskPojo.getTaskRemainingHrs()));
        	}
        }
		
	}
	
	/**
	 * To return Task details
	 * @return
	 */
	private void getTaskDetails(String taskId) {
		ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
		taskPojo = (TaskPojo) fb.getTaskDetails(taskId);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_edit, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Method to save the modified task details
	 * @param view
	 */
	public void saveTaskEdit(View view) {
		String taskId = ((TextView) findViewById(R.id.tview_task_edit_taskid_value_id)).getText().toString();
		String title = ((TextView) findViewById(R.id.et_task_edit_tasktitle_value_id)).getText().toString();
        String description = ((TextView) findViewById(R.id.tview_task_edit_taskdescription_value_id)).getText().toString();
        String estHrs = ((Spinner)findViewById(R.id.spinner_edit_task_est_hrs_id)).getSelectedItem().toString();
        String remHrs = ((Spinner)findViewById(R.id.spinner_edit_task_rem_hrs_id)).getSelectedItem().toString();
        ProjectTrackerDatabase fb = new ProjectTrackerDatabase(getApplicationContext());
        if (fb.saveTaskEdit(taskId, title, description, estHrs, remHrs) == 1) {
        	Toast.makeText(getApplicationContext(), "Task Updated !!", Toast.LENGTH_LONG).show();
        } else {
        	Toast.makeText(getApplicationContext(), "Task Update Failed !!", Toast.LENGTH_LONG).show();
        }
	}

}
